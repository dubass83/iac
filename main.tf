# https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest
resource "kubernetes_manifest" "root_app" {

  manifest = {
    apiVersion = var.root_app_crd_api_version
    kind       = "Application"
    metadata = {
      name      = "system-apps"
      namespace = var.kubernetes_namespace
      labels = {
        "app.kubernetes.io/managed-by" = "Terraform"
      }
      finalizers = [
        "resources-finalizer.argocd.argoproj.io",
      ]
    }
    spec = {
      project = "default"
      destination = {
        namespace = var.kubernetes_namespace
        server    = "https://kubernetes.default.svc"
      }
      source = {
        repoURL        = var.root_app_repository_url
        path           = "."
        targetRevision = var.root_app_repository_ref
        helm = {
          parameters = concat([
            {
              name  = "namespace"
              value = var.kubernetes_namespace
            },
          ], var.root_app_chart_parameters)
          values = var.root_app_chart_values
        }
      }
      syncPolicy = {
        automated = {
          prune    = true
          selfHeal = false
        }
      }
    }
  }

  wait {
    fields = {
      "status.sync.status" = "Synced"
    }
  }

  timeouts {
    create = "30m"
    delete = "30m"
  }

  field_manager {
    force_conflicts = true
  }
}

