variable "kubernetes_namespace" {
  description = "The Kubernetes namespace Argo CD resources are deployed to"
  type        = string
  default     = "argo-system"
}

variable "root_app_crd_api_version" {
  description = "The Kubernetes ApiVersion of the Argo CD root app custom resource"
  type        = string
  default     = "argoproj.io/v1alpha1"
}

variable "root_app_repository_url" {
  description = "The git repository URL where your root app (app of apps) is hosted (fork from https://gitlab.com/vlasman/root-app-chart)"
  type        = string
}

variable "root_app_repository_ref" {
  description = "The git repository reference (branch/tag/HEAD) of your root app (app of apps)"
  type        = string
  default     = "HEAD"
}

variable "root_app_chart_parameters" {
  description = "Extra Helm chart parameters to pass to your root app (app of apps)"
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "root_app_chart_values" {
  description = "Extra Helm chart values to pass to your root app (app of apps)"
  type        = string
  default     = ""
}
